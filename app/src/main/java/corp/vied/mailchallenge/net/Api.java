package corp.vied.mailchallenge.net;

import corp.vied.mailchallenge.model.EmailVerificationModel;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

import static corp.vied.mailchallenge.net.ApiConstant.API_KEY;
import static corp.vied.mailchallenge.net.ApiConstant.EMAIL;

public interface Api {
    @GET("/v2/verify")
    Observable<EmailVerificationModel> verifyEmail(@Query(EMAIL) String email, @Query(API_KEY) String apikey);
}
