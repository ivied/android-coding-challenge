package corp.vied.mailchallenge;

import android.app.Application;

import corp.vied.mailchallenge.dagger.AppComponent;
import corp.vied.mailchallenge.dagger.module.ContextModule;
import corp.vied.mailchallenge.dagger.DaggerAppComponent;


public class App extends Application {

    private static App sInstance;
    private static AppComponent sAppComponent;

    public static App getInstance() {
        return sInstance;
    }

    public static AppComponent getAppComponent() {
        return sAppComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;

        sAppComponent = DaggerAppComponent.builder().contextModule(new ContextModule(this)).build();
    }
}
