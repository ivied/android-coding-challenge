package corp.vied.mailchallenge;

import android.annotation.SuppressLint;
import android.support.annotation.IdRes;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;

public class BaseActivity  extends MvpAppCompatActivity {

    @SuppressLint("ResourceType")
    public void showToast(@IdRes int id) {
        Toast.makeText(this, id, Toast.LENGTH_SHORT).show();
    }

    public void showToast(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }
}
