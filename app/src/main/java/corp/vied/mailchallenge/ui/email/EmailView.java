package corp.vied.mailchallenge.ui.email;

import android.support.annotation.IdRes;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import corp.vied.mailchallenge.model.EmailVerificationModel;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface EmailView extends MvpView {

    @StateStrategyType(AddToEndSingleStrategy.class)
    void setEmailStatus(EmailVerificationModel.Reason reason);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void showProgress(boolean status);

    @StateStrategyType(SkipStrategy.class)
    void showToast(@IdRes int id);

    @StateStrategyType(SkipStrategy.class)
    void showToast(String error);
}
