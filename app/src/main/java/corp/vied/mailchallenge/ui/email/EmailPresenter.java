package corp.vied.mailchallenge.ui.email;

import android.os.Handler;
import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;

import java.util.regex.Pattern;

import javax.inject.Inject;

import corp.vied.mailchallenge.App;
import corp.vied.mailchallenge.BasePresenter;
import corp.vied.mailchallenge.model.EmailVerificationModel;
import corp.vied.mailchallenge.net.Api;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static corp.vied.mailchallenge.views.EmailVerifyView.*;

@InjectViewState
public class EmailPresenter extends BasePresenter<EmailView> {

    private static final String APIKEY = "test_470818df5cd6966a07aff243d10edb466408b7672a02e38f350d068075c10a26";

    @Inject
    Api mApi;

    private long lastSymbolEnter;
    private long delay = 500;
    private Handler handler;
    private static final Pattern EMAIL_NAME_PART = Pattern.compile("[a-zA-Z0-9+._%\\-+]{1,256}");
    private static final Pattern EMAIL_DOMAIN_PART = Pattern.compile("[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}(\\.[a-zA-Z0-9][a-zA-Z0-9\\-]{1,25})+");


    EmailPresenter() {
        App.getAppComponent().inject(this);
        handler = new Handler();
    }

    void enterEmail(String email) {
        verifyEmailPostDelay(email);
        lastSymbolEnter = System.currentTimeMillis();

    }

    //Verify email request
    private void verifyEmailInKickBox(String email) {
        getViewState().showProgress(true);
        Disposable disposable = mApi.verifyEmail(email, APIKEY)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        verification -> {
                            getViewState().setEmailStatus(verification.getReason());
                            getViewState().showProgress(false);
                        }, throwable -> {
                            getViewState().setEmailStatus(EmailVerificationModel.Reason.NO_CONNECT);
                            getViewState().showProgress(false);

                        });

        unsubscribeOnDestroy(disposable);
    }

    //Verify email RegExp
    private void verifyEmailPostDelay(String email) {
        handler.postDelayed(() -> {
            if (lastSymbolEnter <= System.currentTimeMillis() - delay) {
                if (!email.contains("@")) {
                    if (TextUtils.isEmpty(email)) {
                        getViewState().setEmailStatus(EmailVerificationModel.Reason.EMPTY_EMAIL);
                    } else if (!EMAIL_NAME_PART.matcher(email.trim()).matches()) {
                        getViewState().setEmailStatus(EmailVerificationModel.Reason.WRONG_NAME);
                    } else {
                        getViewState().setEmailStatus(EmailVerificationModel.Reason.INVALID_EMAIL);
                    }
                } else {
                    if (email.substring(0, 1).equals("@")) {
                        getViewState().setEmailStatus(EmailVerificationModel.Reason.EMPTY_EMAIL);
                    } else {
                        String[] emailParts = email.split("@");

                        String emailNamePart = emailParts[0];
                        String emailDomainPart = emailParts.length > 1 ? emailParts[1] : "";

                        if (!EMAIL_NAME_PART.matcher(emailNamePart.trim()).matches()) {
                            getViewState().setEmailStatus(EmailVerificationModel.Reason.WRONG_NAME);
                        } else if (!EMAIL_DOMAIN_PART.matcher(emailDomainPart.trim()).matches()) {
                            getViewState().setEmailStatus(EmailVerificationModel.Reason.INVALID_DOMAIN);
                        } else {
                            verifyEmailInKickBox(email);
                        }
                    }
                }
            }
        }, delay);
    }


    @Override
    public void attachView(EmailView view) {
        super.attachView(view);
    }

}
