package corp.vied.mailchallenge.ui.email;

import android.os.Bundle;
import com.arellomobile.mvp.presenter.InjectPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import corp.vied.mailchallenge.BaseActivity;
import corp.vied.mailchallenge.R;
import corp.vied.mailchallenge.model.EmailVerificationModel;
import corp.vied.mailchallenge.views.EmailVerifyView;

public class EmailActivity extends BaseActivity implements EmailView {

    @InjectPresenter()
    EmailPresenter mPresenter;

    @BindView(R.id.verifyEmail)
    EmailVerifyView verifyEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email);
        ButterKnife.bind(this);

        verifyEmail.setListener(email -> mPresenter.enterEmail(email));
    }

    @Override
    public void setEmailStatus(EmailVerificationModel.Reason reason) {
        verifyEmail.changeStatus(reason);
    }

    @Override
    public void showProgress(boolean status) {
        verifyEmail.showProgress(status);
    }
}
