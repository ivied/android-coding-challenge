package corp.vied.mailchallenge.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;
import corp.vied.mailchallenge.R;
import corp.vied.mailchallenge.model.EmailVerificationModel;

public class EmailVerifyView extends RelativeLayout {

    @BindView(R.id.email)
    protected AutoCompleteTextView mEmail;
    @BindView(R.id.progressBar)
    protected ProgressBar mProgressBar;
    @BindView(R.id.errorMessage)
    protected TextView mErrorMessage;

    private Context mContext;


    private IEmailTextChangeListener mListener;

    private boolean skip = true;


    public EmailVerifyView(Context context) {
        super(context);
        init(context);
    }

    public EmailVerifyView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public EmailVerifyView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public void init(Context context) {
        mContext = context;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (inflater == null) {
            return;
        }
        View view = inflater.inflate(R.layout.email_verify_view, this);
        ButterKnife.bind(this, view);

        DomainAutoCompleteAdapter adapter = new DomainAutoCompleteAdapter(context);
        mEmail.setAdapter(adapter);
    }

    @OnTextChanged(R.id.email)
    protected void changeEmail() {
        if (!skip) {
            mEmail.setCompoundDrawablesWithIntrinsicBounds(R.drawable.user_success, 0, 0, 0);
            mErrorMessage.setText("");
            mListener.onChangeEmail(mEmail.getText().toString());
        } else {
            skip = false;
        }
    }

    public void setListener(IEmailTextChangeListener listener) {
        mListener = listener;
    }

    public void showProgress(boolean status) {
        mProgressBar.setVisibility(status ? VISIBLE : GONE);
    }

    public void changeStatus(EmailVerificationModel.Reason reason) {
        switch (reason) {
            case ACCEPTED:
                mEmail.setCompoundDrawablesWithIntrinsicBounds(R.drawable.user_success, 0, R.drawable.email_verified, 0);
                mErrorMessage.setText("");
                break;
            case WRONG_NAME:
                mEmail.setCompoundDrawablesWithIntrinsicBounds(R.drawable.user_failed, 0, R.drawable.email_verify_failed, 0);
                mErrorMessage.setText(mContext.getResources().getString(R.string.wrong_email_name));
                break;
            case INVALID_DOMAIN:
                mEmail.setCompoundDrawablesWithIntrinsicBounds(R.drawable.user_failed, 0, R.drawable.email_verify_failed, 0);
                mErrorMessage.setText(mContext.getResources().getString(R.string.wrong_email_domain));
                break;
            case EMPTY_EMAIL:
                mEmail.setCompoundDrawablesWithIntrinsicBounds(R.drawable.user_failed, 0, R.drawable.email_verify_failed, 0);
                mErrorMessage.setText(mContext.getResources().getString(R.string.empty_email));
                break;
            case INVALID_EMAIL:
                mEmail.setCompoundDrawablesWithIntrinsicBounds(R.drawable.user_failed, 0, R.drawable.email_verify_failed, 0);
                mErrorMessage.setText(mContext.getResources().getString(R.string.wrong_email));
                break;
            case INVALID_SMTP:
                mEmail.setCompoundDrawablesWithIntrinsicBounds(R.drawable.user_failed, 0, R.drawable.email_verify_failed, 0);
                mErrorMessage.setText(mContext.getResources().getString(R.string.wrong_smtp));
                break;
            case REJECTED:
                mEmail.setCompoundDrawablesWithIntrinsicBounds(R.drawable.user_failed, 0, R.drawable.email_verify_failed, 0);
                mErrorMessage.setText(mContext.getResources().getString(R.string.wrong_email));
                break;
            case NO_CONNECT:
                mEmail.setCompoundDrawablesWithIntrinsicBounds(R.drawable.user_success, 0, R.drawable.no_internet, 0);
                mErrorMessage.setText(mContext.getResources().getString(R.string.connection_problem));
                break;
            case TIMEOUT:
            case UNEXPECTED_ERROR:
            case UNAVAILABLE_SMTP:
                mEmail.setCompoundDrawablesWithIntrinsicBounds(R.drawable.user_success, 0, R.drawable.email_verify_failed, 0);
                mErrorMessage.setText("");
                break;
        }

    }

    public interface IEmailTextChangeListener {
        void onChangeEmail(String email);
    }
}
