package corp.vied.mailchallenge.views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import corp.vied.mailchallenge.R;

public class DomainAutoCompleteAdapter extends BaseAdapter implements Filterable {

    private static final int MAX_RESULTS = 5;

    private final Context mContext;
    private List<String> mResults;

    DomainAutoCompleteAdapter(Context context) {
        mContext = context;
        mResults = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return mResults.size();
    }

    @Override
    public String getItem(int index) {
        return mResults.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    static class ViewHolder {
        TextView domainItem;
        View divider;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.domain_hint_item, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.domainItem = convertView.findViewById(R.id.domainTv);
            viewHolder.divider = convertView.findViewById(R.id.divider);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        initUi(position, viewHolder);

        return convertView;
    }

    private void initUi(int position, ViewHolder viewHolder) {

        viewHolder.divider.setVisibility(position == getCount() - 1 ? View.GONE : View.VISIBLE);
        viewHolder.domainItem.setText(getItem(position));
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    String email = constraint.toString();
                    String[] partsEmail = email.split("@");
                    String name = partsEmail[0];
                    String domain = partsEmail.length == 1 ? email.contains("@") ? "" : null : partsEmail[1];

                    changeList(name);

                    if (domain != null) {
                        List<String> filteredList = filterList(domain);
                        filterResults.values = filteredList;
                        filterResults.count = filteredList.size();
                    } else {
                        filterResults.values = new ArrayList<>();
                        filterResults.count = 0;
                    }
                }

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };
    }

    //Generate list emails
    private void changeList(String nameEmail) {
        mResults.clear();
        mResults.add(nameEmail + "@gmail.com");
        mResults.add(nameEmail + "@yahoo.com");
        mResults.add(nameEmail + "@gmail.co.uk");
        mResults.add(nameEmail + "@mail.ru");
        mResults.add(nameEmail + "@ukr.net");
        mResults.add(nameEmail + "@hotmail.com");
    }

    //Filter list
    private List<String> filterList(String domainName) {
        List<String> result = new ArrayList<>();
        for (int i = 0; i < mResults.size(); i++) {
            if (mResults.get(i).split("@")[1].contains(domainName)) {
                result.add(mResults.get(i));
                if (result.size() == MAX_RESULTS) {
                    break;
                }
            }
        }
        mResults.clear();
        mResults.addAll(result);
        return result;
    }
}
