package corp.vied.mailchallenge.dagger.module;

import javax.inject.Singleton;

import corp.vied.mailchallenge.net.Api;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module(includes = {RetrofitModule.class})
public class ApiModule {

    @Provides
    @Singleton
    public Api provideApi(Retrofit retrofit) {
        return retrofit.create(Api.class);
    }
}
