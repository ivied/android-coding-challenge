package corp.vied.mailchallenge.dagger;


import android.content.Context;

import corp.vied.mailchallenge.dagger.module.ApiModule;
import corp.vied.mailchallenge.dagger.module.ContextModule;
import corp.vied.mailchallenge.ui.email.EmailPresenter;
import dagger.Component;

import javax.inject.Singleton;


@Singleton
@Component(modules = {ContextModule.class, ApiModule.class})
public interface AppComponent {
    Context getContext();

    void inject(EmailPresenter loginPresenter);
}

