package corp.vied.mailchallenge.model;

import com.google.gson.annotations.SerializedName;

public class EmailVerificationModel {


    private Result result;
    private Boolean success;
    private Reason reason;

    public EmailVerificationModel(Result result, Boolean success, Reason reason) {
        this.result = result;
        this.success = success;
        this.reason = reason;
    }

    public Result getResult() {
        return result;
    }

    public Boolean getSuccess() {
        return success;
    }

    public Reason getReason() {
        return reason;
    }


    public enum Reason {
        @SerializedName("accepted_email")
        ACCEPTED,
        @SerializedName("rejected_email")
         REJECTED,
        @SerializedName("invalid_domain")
        INVALID_DOMAIN,
        @SerializedName("invalid_email")
        INVALID_EMAIL,
        @SerializedName("invalid_smtp")
        INVALID_SMTP,
        @SerializedName("low_quality")
        LOW_QUALITY,
        @SerializedName("low_deliverability")
        LOW_DELIVERABILITY,
        @SerializedName("timeout")
        TIMEOUT,
        @SerializedName("unexpected_error")
        UNEXPECTED_ERROR,
        @SerializedName("no_connect")
        NO_CONNECT,
        @SerializedName("unavailable_smtp")
        UNAVAILABLE_SMTP,
        @SerializedName("wrong_name")
        WRONG_NAME,
        @SerializedName("empty_email")
        EMPTY_EMAIL
    }

    public enum Result {
        @SerializedName("deliverable")
        DELIVERABLE,
        @SerializedName("undeliverable")
        UNDELIVERABLE,
        @SerializedName("risky")
        RISKY,
        @SerializedName("unknown")
        UNKNOWN
    }
}
